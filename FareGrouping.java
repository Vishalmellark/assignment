 

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FareGrouping {
    public static void main(String[] args) {
        String[] routes = {"13", "13-C", "342-R", "146-Q", "27", "29-A", "215-U", "27-E1", "13J", "SBS-D34G"};
        int[] fares = {10, 15, 10, 10, 15, 12, 12, 15, 12, 10};
        Map<Integer, List<String>> fareGroups = groupByFare(routes, fares);

        System.out.println("Fare\tRoute");
        for (Map.Entry<Integer, List<String>> entry : fareGroups.entrySet()) {
            System.out.print(entry.getKey() + "\t" + entry.getValue());
            System.out.println();
        }
    }

    private static Map<Integer, List<String>> groupByFare(String[] routes, int[] fares) {
        Map<Integer, List<String>> fareGroups = new HashMap<>();

        for (int i = 0; i < routes.length; i++) {
            int fare = fares[i];
            String route = routes[i];

            if (!fareGroups.containsKey(fare)) {
                fareGroups.put(fare, new ArrayList<>());
            }

            fareGroups.get(fare).add(route);
        }

        return fareGroups;
    }
}