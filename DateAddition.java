
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateAddition {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: java DateAddition <date> <days>");
            return;
        }

        String inputDate = args[0];
        int daysToAdd = Integer.parseInt(args[1]);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(inputDate);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_MONTH, daysToAdd);

            Date newDate = calendar.getTime();
            String outputDate = sdf.format(newDate);

            System.out.println("Input Date: " + inputDate);
            System.out.println("Days to Add: " + daysToAdd);
            System.out.println("Output Date: " + outputDate);

        } catch (ParseException e) {
            System.out.println("Invalid date format. Please use dd-MM-yyyy.");
        }
    }
}