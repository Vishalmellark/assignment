  class LinkedListExample {
    static class Node {
        int data;
        Node next;

        Node(int data) {
            this.data = data;
        }
    }

    Node head;

    void addNode(int data) {
        Node newNode = new Node(data);
        if (head == null) head = newNode;
        else getLastNode().next = newNode;
    }

    Node getLastNode() {
        Node temp = head;
        while (temp.next != null) temp = temp.next;
        return temp;
    }

    void printReverse(Node node) {
        if (node != null) {
            printReverse(node.next);
            System.out.print(node.data + " ");
        }
    }

    void printReverse() {
        printReverse(head);
    }

    void printList() {
        for (Node temp = head; temp != null; temp = temp.next) System.out.print(temp.data + " ");
    }

    public static void main(String[] args) {
        LinkedListExample list = new LinkedListExample();
        for (int i = 1; i <= 5; i++) list.addNode(i);

        System.out.println("Original list:");
        list.printList();

        System.out.println("\nList in reverse order:");
        list.printReverse();
    }
}