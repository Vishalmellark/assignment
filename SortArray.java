
  public class SortArray {
    public static void main(String[] args) {
        int[] array = {0, 1, 2, 1, 2, 0, 2, 0, 1};

        System.out.print("Original Array: ");
        printArray(array);

        sortArray(array);

        System.out.print("\nSorted Array: ");
        printArray(array);
    }

    static void sortArray(int[] array) {
        int low = 0;
        int high = array.length - 1;
        int i = 0;

        while (i <= high) {
            if (array[i] == 0) {
                swap(array, i, low);
                i++;
                low++;
            } else if (array[i] == 1) {
                i++;
            } else {  // array[i] == 2
                swap(array, i, high);
                high--;
            }
        }
    }

    static void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    static void printArray(int[] array) {
        for (int num : array) {
            System.out.print(num + " ");
        }
    }
}
