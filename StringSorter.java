 

import java.util.Arrays;
import java.util.Comparator;

public class StringSorter {
    public static void main(String[] args) {
        String input = "readable content of a page when looking at its layout";
        String[] words = input.split(" ");

        Arrays.sort(words, Comparator.comparingInt(String::length).thenComparing(Comparator.naturalOrder()));

        String output = String.join(" ", words);

        System.out.println("Input: " + input);
        System.out.println("Output: " + output);
    }
}